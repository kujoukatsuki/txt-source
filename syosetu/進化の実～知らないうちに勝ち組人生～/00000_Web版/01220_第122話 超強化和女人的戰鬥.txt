正如蛇神所說，下層襲擊的都是強大的魔物。
等級都在700以上。
正因如此，我也說過要戰鬥了，但是被露易絲拒絕了，所以除了我以外的成員
都和魔物戰鬥著。
是的，露提亞現在也和薩利亞她們一起戰鬥。

「那個，我需要在這嗎？」

不禁脫口而出了，在別人看來應該完全不明白我是為了什麼而在這裡吧。我自己也搞不清楚了。
幸運的是，那個草原階層的下面又成了洞窟的構造，通道也很小，所以很少擔心被大量的魔物一齊襲擊。

「薩莉亞，到你那邊了！」
「嗯，交給我吧！」

阿爾為了迷惑巨大的螳螂魔物『瘋狂‧螳螂Lv：711』而來回移動著，在露出破綻的瞬間，薩利亞用強力的一擊將它擊倒。

「啊啊！」
「貪吃鬼，稍微考慮一下合作......。」
「那是能吃的嗎！？」
「算了。」

露露妮和奧利加醬與『暴君‧馬Lv : 802』臉上浮著噁心血管的馬的魔物戰鬥著。嗯，真的是驢馬大戰啊。
但是露露妮無論對方是馬還是什麼都關係的踢打著那巨大的身軀，奧利加醬配合著這個動作，準確地攻擊其要害。

「露提亞小姐。」
「嗯，『魔王之手』」
「『水激光』」

露易絲和露提亞與『邪惡‧蛇Lv : 900』蛇系統的魔物為對手。
不，與其說是對手，不如說是單方面的殲滅。
至少有將近400級的差距，但是由於露提亞的『魔王之手』而被燒傷了身體，狂暴的邪惡‧蛇的脖子被露易絲用纏繞著『水激光』的劍斬飛。
......。

「果然不需要我了吧？」

我到這裡就沒有進行一場戰鬥！？明明被請求調查的是我！
正在我認真思考的存在意義的時候，將魔物打倒的露易絲走過來。

「不，師傅。正因為有師傅，我們才能安心挑戰高等級的魔物。」
「真的嗎？話雖如此，但我也應該參與戰鬥了。」

但是，由於現狀解決了，所以尊重想要鍛鍊的大家的意願比較好吧。
我無奈的嘆氣，露易絲好像想起了什麼的說。

「啊，師傅。我已經730級了。」
「哈？」
「啊，我升到900級了。」
「騙人的吧！？」

不管怎麼說，露易絲的等級不僅超過了500，還升到了730這個意想不到的數字。
而且薩利亞也等級達到了900......。

「薩利亞是魔物還能理解，不過，露易絲是怎麼回事呢！？人類的最高等級不是超過500後就很難再上升嗎！？」
「不過，已經730了。」
「這不是答案啊！」

超過500級的人好像稱為【超越者】，但是露易絲也太超越了吧！？到底發生了什麼？？
對一個勁地吐槽的我，阿爾不好意思的說。

「啊啊......那個......誠一。」
「嗯？」
「...我......687級了啊......。」
「......真的嗎？」
「嗯，嗯。」

阿爾也在不知不覺地進入了【超越者】的行列。這是開玩笑的吧？

「奧利加醬和露露妮呢？」
「......啊，誠一哥哥......我，710。」
「主人，我不清楚！」
「我才對露露妮感覺最不清楚！」

我對奧麗加醬成為【超越者】也不吃驚，但露露妮就不同了。
因為是喝乾湖水的驢子啊！！而且因為肚子還沒飽，這是是怎樣的肚子啊！
相比因為吐槽過度而疲憊不堪的我，露提亞很感動，情緒高漲了。

「誠一。我的等級是651。這樣我也成為了【超越者】的夥伴。」
「那......算了......被狙擊的時候，某種程度上也能安心，這樣好嗎？」
「嗯。這樣一來，澤羅斯的負擔也會減少......而且，爸爸回來的時候，我希望能稍微得到表揚。」
「是嗎......。」

這麼說來，露提亞是「魔王的女兒」，而不是「魔王」啊。總之，露提亞的父親是活著的吧。但不知道在哪裡。
這邊的詳細的話我還沒有好好地聽......等這個地下城的事解決後，在打聽一下吧。
結果，從等級來看，除了我之外，所有人都進入了「超越者」的行列。魯魯妮？反正也一定變成「超越者」吧。如果不是那樣，驢才不會有這種意味不明的存在！
我是因為長袍的效果等級才難以上升的吧。嘛，是可以脫掉，不過一直都穿著，而且繼續變得不像「人」我也會很困擾。
雖然沒想到會變成「超越者」的集團，但我重新考慮一下，變強能保護自己是好事，就繼續前進。
突然，奧麗加醬停了下來。

「......誠一哥哥。」
「嗯？怎麼了？」
「......這面牆，大概能破壞它。」
「誒？」

奧麗加醬所指的牆壁，單從外表來看我覺得和其他的牆壁沒有什麼區別。相比之下，我在黑龍神的迷宮破壞了不可能破壞的牆壁。
嗯，還是忘了吧！
我決心把黑龍神迷宮發生的事封印起來。奧麗加醬對著那牆壁用苦無攻擊。

「哦，......。」

結果，正如奧麗加醬所說的那樣，牆壁簡單地破壞了，裡面發現一個的小房間。
房間中央放著有豪華裝飾的寶箱。......這個寶箱不是魔物吧？
我想起現在父親那裡的寶箱，奧麗加醬小心翼翼地進入了小房間。

「......嗯。這個房間和寶箱也沒有陷阱。」
「咦？奧麗加醬擁有陷阱感知系的技能嗎？」
「......嗯。我在凱爾帝國的時候，和暗殺技能一起記住了。」
「......是嗎？讓你想起不好的事情了。但是，這個能力對潛入迷宮的冒險者們來看的話是非常有用的能力。」
「......嗯。沒關係，阿爾特利亞姐姐。現在已經不介意了。比起這個，能幫到誠一哥哥我很高興。」

阿爾臉上露出不好意思的表情，奧麗加醬搖了頭，微微挺胸。
我不由得撫摸著那個可愛的小腦袋，重新走進了小房間。

「那麼......奧麗加醬確認這個寶箱好像沒有特別的陷阱，就打開它吧。」

我毫不猶豫的打開寶箱，裡面是一個和奧麗加醬的使用的苦無稍微形狀不同的苦無。

「......這是，「苦無」吧。」
「是啊......那麼，有什麼能力呢？」

『咒蛇之苦無』......寄宿著咒蛇能力的苦無。傳說級武器。能通過自身的魔力對苦無賦予麻痺、毒、石化的三個異常狀態。能根據持有者的意願不斷增加，投出去的苦無，能按持有者的意思消除它。

「......不是相當凶惡的武器嗎？這個......。」
「是啊......與傳說級相稱的性能吧。即便如此，出現傳說級武器這一點就知道這個迷宮的難度是相當高的。」

對迷宮十分瞭解的阿爾和露易斯做出評價。
確實，我也覺得這個苦無很強。
只要向苦無注入魔力，就能賦予麻痺、毒和石化三個異常狀態很強大的，但比起根據主人的意志，數量也會增加更加不妙。
也就是說，可以無限的增加。
而且投出去的苦無不用回收也會自動消失......。
意想不到的武器出現而感到吃驚，奧麗加醬眼睛閃閃發光看著這個苦無。

「拿著，奧利加醬。」
「......？為什麼交給我？」
「為什麼......奧利加醬發現了，當然是奧利加醬用的吧？」
「但是，但是......。」
「難得奧利加醬也在使用苦無，奧利加醬不用的話誰用呢！」

奧利加醬好像有所顧慮，薩莉亞也跟我一起說服她。
不知道該怎麼辦才好，奧利加醬看了看阿爾她們，阿爾她們微笑著點點頭。

「......真的好嗎？」
「剛才也說過了，找到這個房間的是奧利加醬，好不容易得到的是奧利加醬使用的武器。」
「......嗯。我知道了。......謝謝你。」

奧利加收起苦無，高興地笑了起來。
看到這個，露提亞有點羨慕的說。

「真好啊......我基本上是用魔法來戰鬥的，所以不會有那麼強的武器啊......。」
「是嗎？雖然我的哥哥也是魔法使的，但是可以提高魔法的威力的權杖和增加魔力制御能力的道具也是有幾個的。雖然不知道在這個迷宮能不能得到的，但是即使是魔法使也能有很強的道具的。」
「是嗎？那樣的話......找一下寶箱也是可以的吧？」

從露易絲那得到了建議的露提亞似乎有了別的目標。
的確通過提高等級來變強是很重要的，但同時擁有強力的武器和道具是再好不過的。

「那樣的話我也去找找看吧。如果能拿到手甲之類的武器就好了......。」
「是啊......」我也想要穿在腳上的武器......。」

好像被露提亞觸動一樣，薩利亞和露露妮也想要武器和道具。
如果入手各自的武器的話，薩利亞也會變得更強，但是，露露妮到底會怎樣反而很害怕。那傢伙，到底要發展成什麼？本來應該是為了當坐騎而買的。
不僅僅是露提亞，薩利亞和露露妮也想要武器或道具嗎......。
如果我參加戰鬥的話道具很容易就到手了......想辦法準備好三個人的道具和武器吧。
即便如此，掉落的道具會與奧麗加醬拿到的武器一樣含有『蛇』嗎？。
這個迷宮中與『蛇』確實有著很大的關係吧。
只是，不知道那是為什麼......。
再也沒找到小房間的我們，繼續向著通道前進。
於是一路上又被魔物襲擊了，不過拿著新武器的奧麗加醬非常活躍。

「......奧義『苦獄』」

奧麗加醬不斷變出剛到手的『咒蛇的苦無』，將它們一起射向襲擊過來的灰色鱗和紅色眼睛的『賢蛇Lv : 855』。
最開始的時候，賢蛇還能靠口裡吐出的水、火焰或者雷擊來阻擋，但是苦無不僅沒有減少，還繼續增加，最終應付不了的賢蛇全身刺滿了苦無。

「沙，啊，啊啊，啊......。」

賢蛇發出很大的叫聲後，從那口中吹出了紫色的泡沫，從尾巴開始漸漸變為灰色────最終完全變成了石像。
雖然已經死了，但是因為沒有光的粒子，所以阿爾用斧子把它劈開。

「哎啊、哈。」

賢者蛇破碎的身體慢慢變成光的粒子消失了。
之後，留下了幾塊鱗片和一個寶箱。

「掉落道具了！」
「真的。而且不僅僅是素材......奧麗加醬之後又掉落武器真的太幸運了。」

阿爾十分佩服的樣子，把鱗片和寶箱撿起來。
順便說一下，鱗片在『上級鑑定』中是這樣顯示。

『賢蛇之鱗』......賢蛇的鱗片。具有很高的魔法耐性，因為非常輕，作為防具很優秀。

我因為有長袍所以不用考慮防具，不過，從說明來看是很有用的素材啊。
然後最在意的寶箱打開了......。

「項鏈嗎......。」

那是帶有紅寶石的十分高雅的項鏈。也試著鑑定一下吧。

「賢者的項鏈」......傳說級的裝飾品。大大提升魔法耐性、魔法威力。使魔力操作變得更加容易，使用魔法時的魔力消耗量減少。

多麼湊巧的道具啊。
沒想到馬上就出現露提亞所要求的道具。
不僅是我，薩利亞她們也對這樣的效果感到驚訝。

「這已經不是一般幸運了吧？為什麼會得到這麼簡單得到想要的東西呢......。」
「超幸運？」

阿爾呆呆地這麼說，奧麗加醬不可思議地歪了歪頭。

「啊啊......露提亞，這是你想要的東西哦。」
「誒？但是......好嗎？明明是兩人打到的魔物......。」
「是的。比起魔法，我更擅長接近戰......。」
「......因為我已經得到了。」
「是嗎......這樣的話......。」

本來就是露提亞所追求的道具，所以沒有特別爭論，就成了露提亞的東西。
因為露提亞是穿著黑色的禮服，與閃耀著紅寶石光芒的項鏈十分搭配。
就這樣，不僅是奧利加醬，露提亞也獲得了新道具，變得更積極地參加了戰鬥。
露提亞在戴著項鏈下使用『魔王之手』的時候，發出與剛才無法比擬的威力，包括露提亞在內所有人都十分吃驚。果然裝備是很重要的。
不管怎樣說，連續掉落道具的事也沒有發生，我們終於到達深處的大房間。

「那麼，這裡是......。」
「誠一，那裡的大門，又有BOSS戰，是吧？」

就像薩利亞所說的，房間前有一扇很厚的大門。

「BOSS......大概也是蛇系統的魔物吧。」
『────說對了』
「咦？」

突然在房間響起的聲音，使我們嚇了一跳。
於是房間的中央部分出現一個不明所以的黑色漩渦，從那出現了一個魔物。
超巨大的蛇的下半身帶有斑點花紋的藍金屬色和黑色的金屬鱗片。
然後────。

『如你們所預料的那樣──人家是蛇的魔物！』

────上半身是肌肉發達的大猩猩。

「不，這是大猩猩的魔物才對吧！？」

那身影我情不自禁的吐槽了。
確實下半身是蛇！明顯的上半身的衝擊比較大吧！
那個大猩猩與薩利亞的種族『凱撒猩猩』不同，藍色的體毛看起來十分涼快。
但是，那個肉體是連大猩猩狀態的薩莉亞......猩亞也會顯得毫不遜色。
雙臂上戴著和下半身一樣的藍金屬色的設計精緻的籠手。
不顧吃驚的我們，眼前的大猩猩打了下響指，周圍又出現了黑色的旋渦，從裡面出現了大量的蛇系統的魔物。
魔物出現的同時，我們的背後也出現厚厚的門。

『那麼，這樣就沒有逃避的路了。我就是這個房間的BOSS。不能讓你們繼續前進了。......小的們，去吧！』
『是，大姐頭！』

受眼前的大猩猩的命令，蛇的魔物一齊向我們襲來。
阿爾她們即刻進入臨戰狀態，不允許魔物的接近。
在中途得到武器和強化道具的奧麗加醬和露提亞，攻擊特別的強大，而且十分適合廣範圍的殲滅，可以說是大顯身手。
大猩猩的出現使我目瞪口呆，我在恢復正常後，立刻發動『上級鑑定』。

『巨大‧猩蟒（Giant Anakong）Lv:1800』

太強了啊啊啊啊啊！
什麼啊！？這個世界裡的大猩猩都這麼強嗎！？不僅比薩利亞的等級高，甚至比澤阿納斯還要高。
而且名字說明混在一起，蛇還是大猩猩好好說清楚啊！
在我對鑑定結果感到驚訝的時候，發現了我的存在的大猩猩的魔物──猩蟒睜大了眼睛。

『嗯！？那，那邊的你......』
「嗯？你叫我嗎？」

不知道發生什麼事的我歪了歪脖子，猩蟒不知為何臉紅了。

『......這不是有好的雄性嗎？嗯，我很喜歡！你，來當人家的老公吧！』
「..................哈？」
「「你這大猩猩在說什麼啊啊啊啊啊啊啊啊！」」

不顧吃驚的我，阿爾和露露妮以十分的氣勢襲擊了猩蟒。
但是，猩蟒用一隻手輕易接住了阿爾的斧子和露露妮的踢擊。

「啊！？」
「我的踢擊！？」
『────不要妨礙人家！』

然後一揮雙手把兩個人吹飛了。
但是，他們兩個都在空中受身，所以沒有受傷。
其他地方也展開激烈的戰鬥，我至今還在凝固中。
────為什麼，我會被大猩猩求婚呢？

『好，趕快把這些傢伙收拾然後舉行儀式吧！』
「稍微等一下啊啊啊啊啊啊啊啊！」

我終於忍不住吐槽了。

「為什麼！敵人！而且還是大猩猩！向我求婚！？」
『一點也不奇怪啊。追求強大的雄性是理所當然的吧？』
「出現了啊超野生理論！什麼？從我的身體裡能發出了吸引大猩猩的費洛蒙嗎！？」
『是啊』
「快說這是騙我的啊啊啊啊啊啊！」

多麼限定的費洛蒙啊！誰想要啊！能不能換成其他費洛蒙啊！
猩蟒慢慢接近全力抱頭的我。
這也太奇怪了吧？不只有薩利亞嗎！？我的身體是怎麼了！
露易絲她們拚命地想要阻止猩蟒，但是被其它蛇的魔物妨礙住。
不，這已經是我的戰鬥了吧！？因為完全盯上我了嘛！大猩猩的妻子只要薩利亞就好了！不如說除了薩利亞以外！其他都不接受！
這已經不是露易絲她們修行的情況了，現在正是我戰鬥的時候。

「────誠一才不會交給你！」
『呶！？』

薩利亞以十分驚人的速度打向猩蟒。
但是被猩蟒接受了那個拳頭，然後他直接用另外一隻手毆打回去。
薩利亞也用另一隻手接了下來，就這樣她們倆變成了所謂的四手緊握的狀態。

「咕......。」
『咕！妳，妳是怎麼回事！？』

雖然等級差相差很大，但薩利亞不僅沒有輸給猩蟒，反而將她壓倒。

「......我是......誠一的......新娘子......！」
『妳，妳說是新娘啊啊啊啊啊啊！？」

猩蟒快要被推倒在地上，馬上就繃緊了神情喊到。

『這樣的話......從妳那裡奪走就好啦......！』

果然是超過1000級的魔物，猩蟒很快從不利的形勢下回到了對抗的狀態。
但是，薩利亞也沒有放棄，朝著猩蟒發出頭槌攻擊。

「不會給你的！」
『嘎！？』

不由得放棄了雙手的猩蟒向後踉蹌了幾步，搖頭向她的臉打去。

『不要太得意忘形了！』
「咕！」
「薩利亞！」
「不要過來！」

薩利亞伸出手阻止了想要幫忙的我。

「這是......女人之間的戰鬥......！」
「女人的戰鬥！？」
「是的......我不是贏家，就沒意義了......！」
『咕......！』

那樣說著的薩利亞也作出回禮，向著猩蟒的臉打去。
────從這裡開始的戰鬥變得很可怕。
二者無不相讓，在那個地方激烈地毆打對方。

「為什麼是誠一啊！還有其他好的雄性啊！」
『從外面來的小ㄚ頭知道什麼啊！？被迷宮束縛的人家一點邂逅都沒有啦！妳知道過了三十歲被耽誤的心情嗎......！』

什麼，這個迷宮的情況。話說已經30歲了啊。

「確實妳的心情我不懂......但是誠一絕對不能讓給你！」
『區區一個小ㄚ頭......！這個雄性是會變成人家的東西的哦！』

已經什麼都不知道的我，只能呆呆的看著兩人戰鬥。
不知什麼時候，到剛才為止阿爾她們還有和她們戰鬥的蛇的魔物也看著兩人戰鬥的。

「薩利亞！絕對不會輸的哦！」
「加油，薩利亞大人！」
「那裡，向那裡攻擊。」
「......薩利亞姐姐，加油！」
「好厲害......魔物之間的戰鬥那麼震撼啊......。」
『不要輸啊，大姐頭啊啊啊啊啊啊啊！』
『那位小姑娘挺厲害啊！』
『喂，你在誇獎對方啊！確實對方也很強......但比起那個，大姐頭真的拼盡全力啊！在這歲數也沒有一個男人......嗚......』
『已經快過適婚期了啊......大姐頭！絕對贏了，然後把男人拿到手！』
『......你們給我等著！』
『『『咦咦咦咦咦！？真的十分對不起！』』』

我是在看小品嗎？
哎，什麼？我很奇怪嗎？不理解這個情況的我很奇怪嗎？
完全只有我一個人在狀況外，這也是沒辦法啊。
正因為如此，我再一次將視線轉向薩利亞她們的戰鬥。

「呀呀呀呀呀呀呀呀呀呀呀！」
『哦哦哦哦哦哦哦哦哦哦哦！』

雙方激烈毆打產生的餘波向周圍擴散。
那個衝擊十分可怕，只靠拳頭的風壓就讓周圍的牆壁裂開，地面已經破破爛爛了。
但是因為是迷宮的牆壁，所以馬上被修復了。
眺望著著令人目瞪口呆的場景，一隻蛇的魔物接近我，把尾巴搭在我的肩上。

『小兄弟，真是受歡迎啊！......雖然是大猩猩』
「一點都不高興啊啊啊啊啊！」

果然很奇怪吧！？怎麼想都是！不，不用想也奇怪的啊！
你們原本不是敵人啊！？為什麼那麼友好啊？

『不不，高興吧！姐姐是個好女人！......雖然是大猩猩』
『是啊是啊，長相也不錯！......雖然是大猩猩』
『能讓我們依賴的統率能力啊，超可靠的啊！......雖然是大猩猩』
『做菜很好，手也意外的很靈巧────』
「不過是大猩猩啊啊啊啊啊啊啊啊！？」

不管怎樣的說明都一定跟著大猩猩啊！
不，不是說大猩猩不好。但是也不是這樣的問題......！

『快把那個男人交出來啊啊啊啊啊啊！』
「絕對不要啊啊啊啊啊啊啊啊啊啊啊！」

一般的話，複數女性爭奪自己的話，也許會很高興。這表示那個人很受歡迎。
但是......為什麼是大猩猩啊啊啊啊啊啊啊！？
至少是人類！拜託是人類啊！因為這不是異種族的戀愛嗎？！再說我已經有薩利亞了呀！
在我嘆息的時候，兩人已經是傷痕累累了。

「哈......哈......。」
『呼......呼......』

兩人沒有絲毫大意的互相瞪著對方。
但是，在這個地方的所有人都這樣想的。
────下一招就決勝負了。
兩人的體力都接近極限了，再一招就用盡力氣了。
這時候，猩蟒露出奇怪的笑容說。

『庫庫庫......哈哈哈哈！』
「又什麼有趣的？」
『不，真對不起......沒想到會變得這麼有趣的戰鬥......。把妳當做小姑娘小看了，真的不好意思。妳為了喜歡的雄性，能勉強跟在格上的人家打到這個地步。人家真的要好好表揚妳。只是啊────』
「！」
『────最後還是人家贏哦！』

猩蟒以至今為止更快的速度逼近薩利亞，然後在薩利亞的腹中放出了渾身的一擊。

「嗚！？」
「薩利亞！？」
『那麼，是人家贏了喲！』

我叫喊著，猩蟒確信了勝利。
────但是，薩利亞還沒有放棄。

「抓到你了。」
『什麼？糟糕！！......』
「誠一是......我的啊啊啊啊啊啊啊！」
『啊啊啊啊啊啊啊啊啊啊啊！』
『『『大，大姐頭啊啊啊啊啊！』』』

薩利亞的銳利的上勾拳在猩蟒的下巴上炸裂開了。

把手臂推到天上，定著不懂的薩利亞。
然後，猩蟒──趴倒在地上。


122完
──────────────────────────────---
譯者的話：
上面出現的猩蟒，原文為アナコング，百度發音為Anna Kong（安娜金剛），不過聯繫下文，這個譯名沒有含有蛇的單詞，然後我在網上查了關於蛇的英文單詞，最後查到了綠水蚺（Green Anaconda），也就是說アナコング應該是Ana-kong，蟒蛇和猩猩的合成詞，我@#￥%......&*，神TM日式英語，美紅我給你跪了...
────────────────---
